# Submission Queue

Before opening a ticket for submitting an app, please take your time to check if the app...

 * ... meets our [inclusion citeria](https://f-droid.org/wiki/page/Inclusion_Policy)
 * ... already is in the [fdroiddata repository](https://gitlab.com/search?utf8=%E2%9C%93&snippets=&scope=issues&project_id=2167965)
 * ... already has an [open (or closed) ticket](https://gitlab.com/search?group_id=&project_id=36528&repository_ref=master&scope=issues)

You also can directly open a merge request with the required metadata which will save you and us a lot of time, see the [contribution guideline](https://gitlab.com/fdroid/fdroiddata/blob/master/CONTRIBUTING.md) and [fdroiddata readme](https://gitlab.com/fdroid/fdroiddata/blob/master/README.md)

### Fromer Queues
* [Forum Search (old)](https://f-droid.org/forums/search/)
* [Submission Queue (old)](https://f-droid.org/forums/forum/submission-queue/)
* [Held Submissions (old)](https://f-droid.org/forums/forum/submissions-held/)
* [Completed Submissions (old)](https://f-droid.org/forums/forum/submissions-complete/)